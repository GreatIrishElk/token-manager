import json
from pathlib import Path


class Tokens:
    def __init__(self, path_to_tokens: str = "tokens.json"):
        path_to_tokens = self._find_tokens(path_to_tokens)
        with open(path_to_tokens, 'r') as tokens:
            self._tokens = json.load(tokens)

    def get_token_field(self, field_name):
        return self._tokens.get(field_name)

    @property
    def app_token(self):
        return self.get_token_field("appToken")

    @property
    def consumer_key(self):
        return self.get_token_field('consumerKey')

    @property
    def consumer_secret(self):
        return self.get_token_field('consumerSecret')

    @property
    def oauth_token(self):
        return self.get_token_field('oauthToken')

    @property
    def oauth_secret(self):
        return self.get_token_field('oauthSecret')

    @staticmethod
    def _find_tokens(file_name: str):
        current_dir = Path(__file__).parent
        path_to_tokens = current_dir / file_name
        while not path_to_tokens.is_file():
            current_dir = current_dir.parent
            path_to_tokens = current_dir / file_name
        return path_to_tokens

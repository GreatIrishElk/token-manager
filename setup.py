from distutils.core import setup

setup(name='TokenManager',
      version='1.0',
      description='For managing my API tokens.py',
      author='Simon Murphy (GreatIrishElk)',
      author_email='murphysimon@oulook.com',
      url='https://gitlab.com/GreatIrishElk/token-manager',
      packages=['token_manager'],
      )
